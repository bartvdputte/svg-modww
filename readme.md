# Modulaire werkkledij SIOEN

Proof of concept SVG manipulatie via javascript + Illustrator workflow

## Illustrator

Layers in illustrator worden geëxporteerd naar een < g > element (group) in de svg met als id de naam van de layer.
Er is wel miserie met underscores: die worden niet netjes geëxporteerd. Dashes (-) geven geen probleem.

Layers in layers worden genest als < g > in de parent < g >.

De SVG in de repo is de _ongewijzigde_ export vanuit illustrator!

```
### Gotcha's

- Enkel layers een *unieke* naam geven die een rol spelen in de app. De rest onbenoemd laten.
- Naamgeving layers: dashes als separator, geen underscores, geen spaties, niet starten met nummer, ...
- Contourvlakken krijgen de naam van "parent"-layer gevolgd door "-contour" (vb: "optie-naam" -> "optie-naam-contour") en hebben enkel een fill als appearance opties. Deze dienen enkel om de kleur te veranderen via CSS.
- Hivis WW hebben standaard 2 kleuren en bijgevolg 2 contour layers: base-hiviscolour-contour & base-contrastcolour-contour.
- "striping-options" layer **moet** visible zijn!

Opgepast: Layers moeten een "echte" layer (of sublayer) zijn, geen group in illustrator!!

```

## Manipulatie via jQuery

Het is perfect mogelijk de SVG elementen aan te spreken en te manipuleren via jQuery. De SVG moet wel embedded in de DOM zitten.

.show() en .hide() op de <g> werkt perfect alsook de fill attribuut wijzigen.
