$(function() {
// Init vars
var svgSources = [ // te testen svg's
    "ModWW-bibbrace-NHV",
    "ModWW-coverall-NHV-layflatcollar",
    "ModWW-coverall-NHV-standupcollar",
    "ModWW-jacket-NHV-layflatcollar",
    "ModWW-jacket-NHV-standupcollar",
    "ModWW-trousers-NHV",
    "ModWW-bibbrace-HV",
    "ModWW-coverall-HV-layflatcollar",
    "ModWW-coverall-HV-standupcollar",
    "ModWW-jacket-HV-layflatcollar",
    "ModWW-jacket-HV-standupcollar",
    "ModWW-trousers-HV"
];
var $svgHolder = $('<div id="svgHolder"></div>'); // wrapper voor de geladen svg's in te stockeren (in memory)
var counter = 0; // aantal tests teller
var domCounter = 0; //nodig omdat we deferred niet proper werkt...

// Load SVG's via promises into $svgHolder so we can test for layers.
$.when(
    svgSources.forEach(function(item) {
        $.get("../svg/" + item + ".svg", function(svg) {
            $svgHolder.append( jQuery(svg).find('svg').attr("id", item) );
            testSVG(item);
            domCounter++; // hack voor de checkForErrorsTimer functie
        });
    })
).done(function() {
    console.log("All loaded. Starting tests");
    checkForErrorsTimer(); // zelfs met deferreds werkt dit op één of andere manier dit zoals verwacht :-( ==> recursieve timer functie
});

function testSVG(svg) {
    _writeHeader(svg);
    var layers = [];
    switch (svg) {
        case "ModWW-bibbrace-NHV":
            layers = ["base", "base-contour", "pocket-flaps-in-other-colour", "pocket-flaps-in-other-colour-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "reflective-piping", "refl ", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-in-other-colour", "large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "large-pocket-with-flap-on-left-thigh-with-reflective-piping", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-pocket-with-flap-on-left-thigh-with-reflective-piping", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-in-other-colour", "rulerpocket-with-flap-on-right-thigh-in-other-colour-contour", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "rulerpocket-with-flap-on-right-thigh-reflective-piping", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-in-other-colour", "phone-pocket-with-flap-on-right-thigh-in-other-colour-contour", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh-reflective-piping", "patched-knee-pockets", "patched-knee-pockets-in-other-colour", "patched-knee-pockets-in-other-colour-contour", "integrated-knee-pockets", "integrated-knee-pockets-in-other-colour ", "integrated-knee-pockets-in-other-colour-contour", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "left-back-pocket-with-flap-in-other-colour", "left-back-pocket-with-flap-in-other-colour-contour", "right-back-pocket-with-flap", "right-back-pocket-with-flap-in-other-colour", "right-back-pocket-with-flap-in-other-colour-contour", "striping-options", "R80", "R41", "R33 ", "R24", "R20", "R14", "R13", "R10"];
        break;
        case "ModWW-coverall-NHV-layflatcollar":
            layers = ["base", "base-contour", "colour-shoulders", "colour-shoulders-contour", "colour-chestpocket-flaps", "colour-chestpocket-flaps-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "reflective-piping", "gas-detector-loop-right", "gas-detector-loop-left", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-colour", "pocket-sleeve-with-flap-colour-contour", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "pocket-sleeve-with-flap-reflective-piping", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-in-other-colour", "large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "large-pocket-with-flap-on-left-thigh-with-reflective-piping", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-pocket-with-flap-on-left-thigh-with-reflective-piping", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-in-other-colour", "rulerpocket-with-flap-on-right-thigh-in-other-colour-contour", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "rulerpocket-with-flap-on-right-thigh-reflective-piping", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-in-other-colour", "phone-pocket-with-flap-on-right-thigh-in-other-colour-contour", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh-reflective-piping", "patched-knee-pockets", "patched-knee-pockets-in-other-colour", "patched-knee-pockets-in-other-colour-contour", "integrated-knee-pockets", "integrated-knee-pockets-in-other-colour", "integrated-knee-pockets-in-other-colour-contour", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "left-back-pocket-with-flap-in-other-colour", "left-back-pocket-with-flap-in-other-colour-contour", "right-back-pocket-with-flap", "right-back-pocket-with-flap-in-other-colour", "right-back-pocket-with-flap-in-other-colour-contour", "striping-options", "R74", "R73", "R70", "R69", "R64", "R63", "R60", "R58", "R47", "R46", "R45", "R41", "R39", "R33", "R34", "R30", "R24", "R23", "R20", "R14", "R13", "R10"];
        break;
        case "ModWW-coverall-NHV-standupcollar":
            layers = ["base", "base-contour", "colour-shoulders", "colour-shoulders-contour", "colour-chestpocket-flaps", "colour-chestpocket-flaps-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "reflective-piping", "gas-detector-loop-right", "gas-detector-loop-left", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-colour", "pocket-sleeve-with-flap-colour-contour", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "pocket-sleeve-with-flap-reflective-piping", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-in-other-colour", "large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "large-pocket-with-flap-on-left-thigh-with-reflective-piping", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-pocket-with-flap-on-left-thigh-with-reflective-piping", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-in-other-colour", "rulerpocket-with-flap-on-right-thigh-in-other-colour-contour", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "rulerpocket-with-flap-on-right-thigh-reflective-piping", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-in-other-colour", "phone-pocket-with-flap-on-right-thigh-in-other-colour-contour", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh-reflective-piping", "patched-knee-pockets", "patched-knee-pockets-in-other-colour", "patched-knee-pockets-in-other-colour-contour", "integrated-knee-pockets", "integrated-knee-pockets-in-other-colour", "integrated-knee-pockets-in-other-colour-contour", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "left-back-pocket-with-flap-in-other-colour", "left-back-pocket-with-flap-in-other-colour-contour", "right-back-pocket-with-flap", "right-back-pocket-with-flap-in-other-colour", "right-back-pocket-with-flap-in-other-colour-contour", "striping-options", "R74", "R73", "R70", "R69", "R64", "R63", "R60", "R58", "R47", "R46", "R45", "R41", "R39", "R33", "R34", "R30", "R24", "R23", "R20", "R14", "R13", "R10"];
        break;
        case "ModWW-jacket-NHV-layflatcollar":
            layers = ["base", "base-contour", "colour-shoulders", "colour-shoulders-contour", "colour-chestpocket-flaps", "colour-chestpocket-flaps-contour", "colour-pocket-flaps", "colour-pocket-flaps-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "reflective-piping", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-colour", "pocket-sleeve-with-flap-colour-contour", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "pocket-sleeve-with-flap-reflective-piping", "elongated-back", "elongated-back-contour", "gas-detector-loop-right", "gas-detector-loop-left", "striping-options", "R74", "R73", "R70", "R69", "R64", "R63", "R60", "R58", "R47", "R41", "R39", "R34", "R33", "R30", "R24", "R23", "R20", "R14", "R13", "R10"];
        break;
        case "ModWW-jacket-NHV-standupcollar":
            layers = ["base", "base-contour", "colour-shoulders", "colour-shoulders-contour", "colour-chestpocket-flaps", "colour-chestpocket-flaps-contour", "colour-pocket-flaps", "colour-pocket-flaps-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "reflective-piping", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-colour", "pocket-sleeve-with-flap-colour-contour", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "pocket-sleeve-with-flap-reflective-piping", "elongated-back", "elongated-back-contour", "gas-detector-loop-right", "gas-detector-loop-left", "striping-options", "R74", "R73", "R70", "R69", "R64", "R63", "R60", "R58", "R47", "R41", "R39", "R34", "R33", "R30", "R24", "R23", "R20", "R14", "R13", "R10"];
        break;
        case "ModWW-trousers-NHV":
            layers = ["base", "base-contour", "large-belt-loop-at-back-in-other-colour", "large-belt-loop-at-back-in-other-colour-contour", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-in-other-colour", "large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "large-pocket-with-flap-on-left-thigh-with-reflective-piping", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour", "reinforced-large-pocket-with-flap-on-left-thigh-in-other-colour-contour", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-pocket-with-flap-on-left-thigh-with-reflective-piping", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-in-other-colour", "rulerpocket-with-flap-on-right-thigh-in-other-colour-contour", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "rulerpocket-with-flap-on-right-thigh-reflective-piping", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-in-other-colour", "phone-pocket-with-flap-on-right-thigh-in-other-colour-contour", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh-reflective-piping", "patched-knee-pockets", "patched-knee-pockets-in-other-colour", "patched-knee-pockets-in-other-colour-contour", "integrated-knee-pockets", "integrated-knee-pockets-in-other-colour", "integrated-knee-pockets-in-other-colour-contour", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "left-back-pocket-with-flap-in-other-colour", "left-back-pocket-with-flap-in-other-colour-contour", "right-back-pocket-with-flap", "right-back-pocket-with-flap-in-other-colour", "right-back-pocket-with-flap-in-other-colour-contour", "striping-options", "R80", "R14", "R13", "R10"];
        break;
        case "ModWW-bibbrace-HV":
            layers = ["base", "base-hiviscolour-contour", "base-contrastcolour-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "patched-knee-pockets", "integrated-knee-pockets", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "right-back-pocket-with-flap", "striping-options", "R10", "R20"];
        break;
        case "ModWW-coverall-HV-layflatcollar":
            layers = ["base", "base-hiviscolour-contour", "base-contrastcolour-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "gas-detector-loop-right", "gas-detector-loop-left", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "patched-knee-pockets", "integrated-knee-pockets", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "right-back-pocket-with-flap", "striping-options", "Sioen-standard", "R20"];
        break;
        case "ModWW-coverall-HV-standupcollar":
            layers = ["base", "base-hiviscolour-contour", "base-contrastcolour-contour", "pocket-flaps-with-contrast-colour-1cm", "pocket-flaps-with-contrast-colour-1cm-contour", "gas-detector-loop-right", "gas-detector-loop-left", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "patched-knee-pockets", "integrated-knee-pockets", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "right-back-pocket-with-flap", "striping-options", "Sioen-standard", "R20"];
        break;
        case "ModWW-jacket-HV-layflatcollar":
            layers = ["base", "base-hiviscolour-contour", "base-contrastcolour-contour", "upper-pocket-flaps-with-contrast-colour-1cm", "upper-pocket-flaps-with-contrast-colour-1cm-contour", "below-pocket-flaps-with-contrast-colour-1cm", "below-pocket-flaps-with-contrast-colour-1cm-contour", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "elongated-back", "elongated-back-contour", "gas-detector-loop-right", "gas-detector-loop-left", "striping-options", "R73", "R20", "R2-2-2", "R70"];
        break;
        case "ModWW-jacket-HV-standupcollar":
            layers = ["base", "base-hiviscolour-contour", "base-contrastcolour-contour", "upper-pocket-flaps-with-contrast-colour-1cm", "upper-pocket-flaps-with-contrast-colour-1cm-contour", "below-pocket-flaps-with-contrast-colour-1cm", "below-pocket-flaps-with-contrast-colour-1cm-contour", "pocket-sleeve-with-flap", "pocket-sleeve-with-flap-contrast-colour-1cm", "pocket-sleeve-with-flap-contrast-colour-1cm-contour", "elongated-back", "elongated-back-contour", "gas-detector-loop-right", "gas-detector-loop-left", "striping-options", "R73", "R20", "R2-2-2", "R70"];
        break;
        case "ModWW-trousers-HV":
            layers = ["base", "base-hiviscolour-contour", "base-contrastcolour-contour", "large-belt-loop-at-back-in-other-colour", "large-belt-loop-at-back-in-other-colour-contour", "large-pocket-with-flap-on-left-thigh", "large-pocket-with-flap-on-left-thigh-with-contrast-strip", "large-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "reinforced-large-pocket-with-flap-on-left-thigh", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip", "reinforced-pocket-with-flap-on-left-thigh-with-contrast-strip-contour", "rulerpocket-without-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh", "rulerpocket-with-flap-on-right-thigh-contrast-strip", "rulerpocket-with-flap-on-right-thigh-contrast-strip-contour", "phone-pocket-with-flap-on-right-thigh", "phone-pocket-with-flap-on-right-thigh-contrast-strip", "phone-pocket-with-flap-on-right-thigh-contrast-strip-contour", "patched-knee-pockets", "integrated-knee-pockets", "flaps-on-sidepockets", "flaps-on-sidepockets-contour", "side-pocket-flaps-in-other-colour", "side-pocket-flaps-in-other-colour-contour", "left-back-pocket-with-flap", "right-back-pocket-with-flap", "striping-options", "Sioen-standard"];
        break;
    }
    layers.forEach(function(entry) {
        _testDOMOccurrence("Check aanwezigheid <strong>" + entry + "</strong> layer", $svgHolder.find("#" + svg + " g#" + entry).length, 1);
    });
}

function _checkForErrors() {
    var $errors = $(".testbox__error");
    var $output = $("#output-stats");
    if ($errors.length > 0) {
        $output.append($errors.length + " errors gevonden! Controleer onderstaande.");
        $(".testbox__success").hide();
    } else {
        $output.append("Alle " + counter + " testen succesvol. Geen errors.")
    }
}
function checkForErrorsTimer() { // recursieve loop die checkt of de SVG's effectief geladen zijn vooraleer de check-overview ( _checkForErrors() ) te starten.
    setTimeout(function() {
        if (domCounter < svgSources.length) {
            checkForErrorsTimer();
        } else {
            _checkForErrors();
            console.log("Check complete");
        }
    }, 10);
}

function _writeHeader(content) {
    $("#output").append("<h2>" + content + "</h2>");
}
function _writeLine(content, status) {
    $("#output").append('<div class="testbox testbox__' + status + '">' + content + "</div>");
}
function _testDOMOccurrence(name, test, expected) {
    if (test == expected) {
        _writeLine(name, "success");
    } else {
        _writeLine(name, "error");
    }
    counter++;
}
});
