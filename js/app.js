$(function() {
    // options
    var optDebug = false;
    var $dbgNoContourSublayer = $("<ul></ul>");
    // init hack
    $(".js-controls").hide();

    // Place linked SVG in DOM zodat we aan SVG elementen kunnen via JS
    $(".js-product-image").each(function() {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        var imgWidth = $img.attr('width');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the replaced SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            // Add replaced image's width to the replaced SVG
            if(typeof imgWidth !== 'undefined') {
                $svg = $svg.attr('width', imgWidth);
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

            // Show UI
            showUI();

            // output SVG debug info
            if (optDebug == true) {
                svgInfo();
            }

        }, 'xml');
    });

    // Show SVG manipulation UI
    function showUI() {
        $(".js-controls").show();
        var $svgObject = $(".product-image-placeholder > svg");

        // populate select met beschikbare layers vlgs afgesproken naamgeving
        $svgObject.find("g").each(function() {
            var layerNaam = this.id;

            // Exclude naamloze layers  && refl layer
            if (layerNaam != "" && layerNaam != "refl" && layerNaam != "striping-options") {
                //Exclude de layers waar "contour" in voorkomt
                var contour = layerNaam.match(/contour/g);

                // We zitten niet in een contour layer
                if (contour === null) {
                    $(".js-svg-groups").append("<option>" + layerNaam + "</option>");

                    // vul debuginfo lijsten
                    if (optDebug == true) {
                        // check of huidige layer een contour sublayer bevat en schrijf weg
                        if ( !hasContourLayer(layerNaam) ) {
                            //console.log(layerNaam);
                            $dbgNoContourSublayer.append("<li>" + layerNaam + "</li>")
                        }
                    }
                }
            }
        });
    }

    // Toon enkel de change-colour button als er, voor de geselecteerde optie, een contour laag is die kan gewisseld worden van kleur
    $(".js-svg-groups").on("change", function() {
        // reset stage & cache DOM elements
        var $btnChangeColour = $(".js-change-option-color");
        var $infoBox = $(".js-layer-info");
        var layerNaam = $(this).find(":selected").text();
        $infoBox.html("");
        $btnChangeColour.hide();

        if( hasContourLayer(layerNaam) ) {
            $btnChangeColour.show();
        } else {
            if (optDebug == true ) {
                showSublayerInfo(layerNaam);
            }
        }
    });

    // Toggle button
    $(".js-group-toggle").on("click", function() {
        var layerId = $(".js-svg-groups").find(":selected").text();
        $("#" + layerId).toggle();
    });

    // Verander kleur
    $(".js-change-option-color").on("click", function() {
        var selectedOption = $(".js-svg-groups").find(":selected").text();
        var $contour = $(".product-image-placeholder svg #" + selectedOption + "-contour").children();
        var currentFill = $contour.attr("fill");

        // demo
        if (currentFill == "#C00D0D") { // rood
            $contour.attr("fill", "#DAFF00"); // groen
        } else if (currentFill == "#DAFF00") {
            $contour.attr("fill", "#02A139");
        } else {
            $contour.attr("fill", "#C00D0D"); // geel
        }
    });

    // controleer als er een sublayer bestaat met verwachte contour layernaam
    function hasContourLayer(parentId) {
        if ($("#" + parentId).find("#" + parentId + "-contour").length == 0) {
            return false;
        } else {
            return true;
        }
    }

    // toon namen van eventuele anders genoemde sublayers
    function showSublayerInfo(parentId) {
        var $infoBox = $(".js-layer-info");
        var $subLayers = $("#" + parentId).find("[id]");

        if ($subLayers.length > 0) {
            $infoBox.html("Geen conventionele contour-sublayer gevonden. Wel:<br>");
            $subLayers.each(function() {
                $infoBox.append( $(this).attr("id") );
            });
        } else {
            $infoBox.html("Deze layer heeft geen sublayers")
        }
    }

    /* debug stuff */

    // debug layernames issues
    function debugLayerNames() {
        $(".product-image-placeholder > svg g").each(function() {
            var layerNaam = this.id;
            if (layerNaam != "" && layerNaam != "refl" && layerNaam != "striping-options") { // Exclude naamloze layers  && refl layer
                var contour = layerNaam.match(/contour/g); //Exclude de layers waar "contour" in voorkomt
                if (contour === null) {
                    $(".js-svg-groups").append("<option>" + layerNaam + "</option>");
                }
            }
        });
    }

    function svgInfo() {
        $("body").append('<hr><h2>Debug info</h2><div id="debug-info"></div>');
        var $dbg = $("#debug-info");
        $dbg.append("<h3>Layers zonder contour sublayer</h3>");
        $dbgNoContourSublayer.appendTo($dbg);
    }

});
